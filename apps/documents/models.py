from django.db import models

# Create your models here.
from core.models.prospecting import ProjectIdea


class Document(models.Model):
    creation_date = models.DateTimeField()
    label = models.CharField(max_length=50)
    notes = models.TextField(null=True)


class ProjectDocument(Document):
    pass

class ProspectingDocument(ProjectDocument):
    project_idea = models.OneToOneField(ProjectIdea)


class Devis(ProspectingDocument):
    structure_costs = models.FloatField()
