from django.db import IntegrityError
from rest_framework import serializers

# Get member with all relevant information
from rest_framework_simplejwt.state import User

from apps.accounts.api import AccountSerializer
from apps.accounts.models import UserAccount
from apps.humanResources.models import Member, MemberPosition, ScholarYear
from core.models.misc import Address
from core.serializers import AddressSerializer, PositionSerializer


class MemberPositionSerializer(serializers.ModelSerializer):
    position = PositionSerializer(many=False, required=False)

    class Meta:
        model = MemberPosition
        fields = ['id','position', 'date_in', 'date_out', 'notes']


class ScholarYearSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScholarYear
        fields = ['year']


class NewMemberPositionSerializer(serializers.Serializer):
    position = PositionSerializer(many=False, required=True)

    class Meta:
        model = MemberPosition
        fields = ['position', 'date_in', 'date_out', 'notes']


class MemberSerializer(serializers.ModelSerializer):

    # we can:
    #   - pass list of positions
    #       - create position if not exist
    #       - update position if changed
    #       - delete position if id and position = null
    #   - pass list of AcademicYear
    #       - CRUD on the same way as previous point

    positions = MemberPositionSerializer(many=True)
    address = AddressSerializer(many=False)
    account = AccountSerializer(many=False)
    scholar_curses = ScholarYearSerializer(many=True)

    class Meta:
        model = Member
        fields = '__all__'

    def update(self, instance, validated_data):
        print(instance.id)
        print(validated_data)

        # only for not array fields
        n_address = None
        if validated_data.get('address', None) is not None:
            serializer = AddressSerializer(data=validated_data['address'])
            if serializer.is_valid():
                n_address = Address(
                    country=serializer.data['country'],
                    zipCode=serializer.data['zipCode'],
                    city=serializer.data['city'],
                    address=serializer.data['address']
                )
                n_address.save()

        instance.address = n_address if n_address is not None else instance.address
        instance.save()

        return instance


# Quick infos on member (List + QuickView)
class MemberListSerializer(serializers.ModelSerializer):
    account = AccountSerializer(many=False)
    address = AddressSerializer(many=False)

    class Meta:
        model = Member
        fields = [
            'first_name', 'surname', 'gender', 'birth_date',  # Person fields
            'address', 'email', 'phone_number', 'age',  # Contact fields
            'account',
            'current_schoolyear',  # computed properties
            'member_id'
        ]


# Create a new member with no mandatory fields => HumanSerializer (contact + person)
class NewMemberSerializer(serializers.ModelSerializer):
    address = AddressSerializer(many=False, required=False, allow_null=True)

    # generate account on the fly attached to member
    generate_account = serializers.BooleanField(default=False, required=False)
    account_pseudo = serializers.CharField(required=False, help_text="pseudo for new user", allow_null=True)

    class Meta:
        model = Member
        fields = [
            'first_name', 'surname', 'gender', 'birth_date',  # Person fields
            'address', 'email', 'phone_number',  # Contact fields
            'generate_account', 'account_pseudo'  # Account field
        ]

    def create(self, validated_data):
        n_account = None
        a = None

        if validated_data.get('address', None) is not None:
            addr = AddressSerializer(data=validated_data.get('address', None))
            if addr.is_valid():
                a = Address.objects.create(**addr.data)

        if validated_data.get('generate_account', False) is not False:
            # create_user
            account_pseudo = validated_data.get('account_pseudo', None)
            if account_pseudo is None and validated_data.get('generate_account', False):
                raise serializers.ValidationError("account_pseudo is required to generate associated account")

            n_password = 'toto123456'  # should be random

            try:
                n_user = User.objects.create(
                    username=account_pseudo,
                )
                n_user.set_password(n_password)


            except IntegrityError:
                raise serializers.ValidationError("Error : integrity ")

            if n_user is not None:
                n_account = UserAccount.objects.create(user_id=n_user.id)
            else:
                # rollback address creation if done.
                if a is not None:
                    a.delete()
                raise serializers.ValidationError("Error occurred while performing account generation")

        member = Member.objects.create(
            first_name=validated_data.get('first_name', None),
            surname=validated_data.get('surname', None),
            gender=validated_data.get('gender', None),

            birth_date=validated_data.get('birth_date', None),
            phone_number=validated_data.get('phone_number', None),
            email=validated_data.get('email', None),
            address_id=a.id if a is not None else None,
            account_id=n_account.id if n_account is not None else None
        )

        return member




