from django.apps import AppConfig


class HumanResourcesConfig(AppConfig):
    name = 'apps.humanResources'
