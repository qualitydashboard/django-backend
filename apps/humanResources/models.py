from django.contrib.auth.models import User
from django.db import models

from apps.accounts.models import UserAccount
from core.models.misc import Address, Person, Position, Contactable


# Create your models here.


class Member(Person, Contactable):
    """
        create Member
            = address (optional)
              person
                - first_name (required)
              account (attachable)
              position (appendable)
              schoolyear (appendable)

    """

    member_id = models.AutoField(primary_key=True)


    account = models.OneToOneField(UserAccount, on_delete=models.deletion.SET_NULL, related_name='member', null=True, blank=True)


    @property
    def current_position(self):
        last_position = self.positions.order_by('-date_in').first()
        if last_position is not None:
            if last_position.date_out is not None:
                return last_position
        return None

    @property
    def current_schoolyear(self):
        last_schoolyear = self.scholar_curses.order_by('-year').first()
        """
        if last_schoolyear is not None:
            if last_schoolyear.date_out is not None:
                return last_schoolyear
        """
        return None


    @property
    def has_account(self):
        return self.account is not None

    #School cursus
    #Association position

    #Addressable

    #Attached account (gpineda + auth services)






class MemberPosition(models.Model):
    """
        History of positions affected to member
    """

    position = models.ForeignKey(
        'core.Position',
        on_delete=models.deletion.SET_NULL,
        related_name="member_positions",
        null=True
    )

    date_in = models.DateField()
    date_out = models.DateField(null=True)
    notes = models.CharField(max_length=240, null=True)


    member = models.ForeignKey('Member', on_delete=models.deletion.SET_NULL, related_name="positions",null=True)



class HrManager(models.Manager):
    def active_positions(self):
        """
            get users with position
        """
        return []



class ScholarYear(models.Model):
    member = models.ForeignKey('Member', related_name='scholar_curses', on_delete=models.deletion.SET_NULL, null=True)
    # department, grade, option
    year = models.PositiveSmallIntegerField()
    # notes
    # associated documents
    pass



