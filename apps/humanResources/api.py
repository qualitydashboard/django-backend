# expose app to api endpoint
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db import IntegrityError
from django.urls import path, include
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers, viewsets, routers, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.accounts.api import AccountSerializer
from apps.accounts.models import UserAccount
from apps.humanResources.models import Member, MemberPosition, ScholarYear
from apps.humanResources.serializers import MemberListSerializer, MemberPositionSerializer, ScholarYearSerializer, \
    NewMemberPositionSerializer, NewMemberSerializer, MemberSerializer
from core.models.misc import Address, Position
from core.serializers import AddressSerializer, UserSerializer, PositionSerializer








class MemberViewSet(viewsets.ModelViewSet):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer

    def list(self, request, *args, **kwargs):
        queryset = Member.objects.all()
        serializer = MemberListSerializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(request_body=NewMemberSerializer, operation_description="Create new Member")
    def create(self, request):
        serializer = NewMemberSerializer(data=request.data)
        if serializer.is_valid():
            m = serializer.save()
            if m is not None:
                return Response(MemberListSerializer(m).data)
                #return Response({'status': 'member {} created'.format(m.member_id)})

        return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post', 'patch', 'put'])
    @swagger_auto_schema(request_body=MemberPositionSerializer, operation_description="CRUD operation on member's position")
    def position(self, request, pk=None):

        # find attached_user
        member = self.get_object()
        if member is None:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:

            if request.method == 'GET':
                # list all member's positions
                queryset = MemberPosition.objects.filter(member_id=member.member_id)
                serializer = MemberPositionSerializer(queryset, many=True)
                return Response(serializer.data)

            if request.method == 'POST':
                # add position to member_id pk
                serializer = NewMemberPositionSerializer(data=request.data)
                print(request.data)
                if serializer.is_valid():
                    print(serializer.data)
                    position, created = Position.objects.get_or_create(title=serializer.data.get('position',dict()).get('title',None))

                    print(position.title)
                    member.positions.create(
                        date_in=request.data.get('date_in',None),
                        date_out=request.data.get('date_out',None),
                        notes=request.data.get('notes',None),
                        position=position
                    )
                    return Response({'status': 'position added to {}'.format(pk)})
                else:
                    return Response(serializer.errors,
                                    status=status.HTTP_400_BAD_REQUEST)

            serializer = MemberPositionSerializer(data=request.data)
            if serializer.is_valid():
                member_position = MemberPosition.objects.get(pk=serializer.data.pk, member_id=pk)
                if member_position is None:
                    # member_position does not exist or isn't related to member pk
                    return Response(status=status.HTTP_404_NOT_FOUND)
                else:
                    if request.method == 'DELETE':
                       member_position.delete()
                       return Response(status=status.HTTP_200_OK)

            return Response('Unknown', status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['patch'])
    @swagger_auto_schema(request_body=MemberPositionSerializer, operation_description="Edit member's position")
    def update_position(self, request, pk=None):

        serializer = MemberPositionSerializer(data=request.data)
        if serializer.is_valid():
            member = self.get_object()

            member.positions.create(
                date_in=serializer.data['date_in'],
                date_out=serializer.data['date_out'],
                title=serializer.data['title']
            )

            return Response({'status': 'position added'})
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post', 'patch', 'put'])
    @swagger_auto_schema(request_body=ScholarYearSerializer, operation_description="Edit scholarship")
    def school_year(self, request, pk=None):
        pass

    @action(detail=True, methods=['post'])
    @swagger_auto_schema(request_body=AccountSerializer, operation_description="Account")
    def account(self, request, pk=None):
        pass


router = routers.DefaultRouter()
router.register('member', MemberViewSet)

urlpatterns = [
    path('', include(router.urls))
]
