from rest_framework import serializers

from apps.accounts.models import UserAccount
from core.serializers import UserSerializer


class AccountSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)

    class Meta:
        model= UserAccount
        fields = ['id','user']
