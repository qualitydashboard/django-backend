# Manage accounts here

# Profiles: Customer, Member, Admin, Guest, ...
# -> custom settings, access log


# Attached auth services (test user credentials to check status), google, fb, twitter ...

# Add API token auth
from django.contrib.auth.models import User
from django.db import models


class AccountManager(models.Manager):
    def matches_credentials(self):
        # return all accounts matching credentials (whatever auth provider)
        pass


class UserAccount(models.Model):
    """
        UserAccount binds to user, settings, type, external accounts, API access
    """

    user = models.OneToOneField(User, on_delete=models.deletion.CASCADE, related_name='account')

    @property
    def type(self):
        # An account can have only one type (separated priviliges)
        if self.member is not None: return 'M'
        if self.customer is not None: return 'C'
        if self.admin is not None: return 'A'   #
        if self.guest is not None: return 'G'   # viewer with temp access (ex: audit)


    @staticmethod
    def matches_credentials():
        pass