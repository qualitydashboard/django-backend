from django.urls import path

from apps.qualityDashboard import views

urlpatterns = [
    path('', views.index, name='index'),
]