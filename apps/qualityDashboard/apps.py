from django.apps import AppConfig


class QualitydashboardConfig(AppConfig):
    name = 'apps.qualityDashboard'
