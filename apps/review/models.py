from django.db import models


# Create your models here.
class DocumentReview(models.Model):
    parent = models.OneToOneField('DocumentReview')
    review_date = models.DateTimeField()
    archived_suggestions = models.TextField()  # JSON formated

    def child(self):
        return DocumentReview.objects.filter(parent=self)

    def get_chain(self):
        pass


class Suggestion(models.Model):
    review = models.ForeignKey('DocumentReview')
    table = models.CharField(max_length=50)
    index = models.IntegerField()
    field = models.CharField(max_length=50)
    n_value = models.TextField()
    note = models.CharField()  # add annotations to explain suggestion

    accepted = models.DateTimeField(null=True)

    def compress(self):
        # serailize to archive current suggestion (JSON)
        pass
