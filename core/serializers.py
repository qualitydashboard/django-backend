from django.contrib.auth.models import User
from rest_framework import serializers

from core.models.misc import Address, Position


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ['country','zipCode','city','address']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username','date_joined']


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = ['position_id','title']