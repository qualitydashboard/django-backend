from django.db import models

# Create your models here.
class ProjectIdea(models.Model):
    """"
        Before starting a survey for a customer, prospect Idea is called ProjectIdea
    """
    context = models.TextField()

class Prospect(models.Model):
    """"
        Person who have a need to answer
    """
    project = models.OneToOneField(ProjectIdea)
    notes = models.TextField()


