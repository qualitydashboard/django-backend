# Create your models here.
from django.db import models

from core.models.prospecting import ProjectIdea


class Project():
    source = models.OneToOneField(ProjectIdea)
    notes = models.TextField()


    def state(self):
        # return project state according to linked documents: Started, Negociating, OutOfDate, Expired, InLate, Done
        pass

