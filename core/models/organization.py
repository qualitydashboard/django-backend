from django.db import models


class Organization(models.Model):
    pass

class Association(Organization):
    pass

class School(Organization):
    pass

class SchoolDepartment(models.Model):
    # MRI, STI, STPI, GSI, ERE
    pass

class SchoolAssociation(Organization):
    # subjects, name ... member can be attached
    pass


class FinancialOrganization(Organization):
    # All meta attached to a financial Organization: URSAAF,
    pass