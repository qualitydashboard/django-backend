from datetime import date

from django.db import models


class Address(models.Model):
    country = models.CharField(max_length=10, default='FR')
    zipCode = models.CharField(max_length=10)
    city = models.CharField(max_length=50)
    address = models.CharField(max_length=120)


class Position(models.Model):
    position_id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=20)

    def current_members_with_position(self):
        matching_members = []
        for member_position in self.member_positions.all():
            if member_position.member is not None:
                matching_members.append(member_position.member)
        return matching_members




class Person(models.Model):
    first_name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50, null=True)

    gender = models.CharField(choices=[
        ('M', 'Male'), ('F', 'Female'),('O', 'Other')
    ], max_length=2, null=True)

    birth_date = models.DateField(null=True)  # Age statistics and more

    @property
    def age(self):
        if self.birth_date is not None:
            today = date.today()
            age = today.year - self.birth_date.year - ((today.month, today.day) <   (self.birth_date.month, self.birth_date.day))
            return age
        return None

    def anonymze(self):
        self.first_name = ""
        self.surname = ""
        self.gender = None

    class Meta:
        abstract = True


class Contactable(models.Model):
    email = models.EmailField(null=True)
    phone_number = models.CharField(max_length=15, null=True)
    address = models.OneToOneField(Address, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        abstract = True
