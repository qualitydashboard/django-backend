from django.urls import path, include
from rest_framework import routers, viewsets

from core.models import Position
from core.serializers import PositionSerializer

router = routers.DefaultRouter()


class PositionViewSet(viewsets.ModelViewSet):
    queryset = Position.objects.all()
    serializer_class = PositionSerializer


router.register('position', PositionViewSet)

urlpatterns = [
    path('', include(router.urls))
]
