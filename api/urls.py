from django.conf.urls import url
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework_simplejwt.views import TokenObtainPairView, TokenVerifyView, TokenRefreshView

schema_view = get_schema_view(
   openapi.Info(
      title="QualityDashboard API",
      default_version='v0.1',
      description="Welcome to the open side of qualityDashboard",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="admin@qualityDashboard.io"),
      license=openapi.License(name="IRIS insa"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)



urlpatterns = [
    # gui
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    # auth
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),

    # apps
    path('RH/', include('apps.humanResources.api')),
    path('core/', include('core.api'))

]


